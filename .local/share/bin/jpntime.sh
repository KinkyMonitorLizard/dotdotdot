#!/bin/sh

# 午前 ("before noon") or 午後 ("after noon")
# 8時42分60秒 hour min sec

TIME=$(date +%H)
if [ $TIME -ge 13 ]; then 
    echo -n "午後"
elif [ $TIME -le 12 ]; then
    echo -n "午前"
fi
echo -n $(date +%I); echo -n "時"
echo -n $(date +%M); echo "分"

DIRTY DOTS DONE DIRT CHEAP

---

## Programs:
##### WM:
* BSPWM
  * https://github.com/baskerville/bspwm

##### Panel:
* Polybar
  * https://github.com/jaagr/polybar - [AUR](https://aur.archlinux.org/packages/polybar/)

##### Launcher:
* dmenu2 
  * https://bitbucket.org/melek/dmenu2 - [AUR](https://aur.archlinux.org/packages/dmenu2/)
* rofi
  * https://github.com/DaveDavenport/rofi

## Fonts:

* Hack Patched Nerd font - [**MIT**](https://github.com/ryanoasis/nerd-fonts/blob/master/LICENSE)
  * https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Hack - [AUR](https://aur.archlinux.org/packages/nerd-fonts-hack/)

* siji - [**GPLv2**](https://github.com/FoxelCode/siji/blob/master/LICENSE)
  * https://github.com/FoxelCode/siji - [AUR](https://aur.archlinux.org/packages/siji-git/)

* tewi - [**MIT**](https://github.com/lucy/tewi-font/blob/master/LICENSE)
  * https://github.com/Lucy/tewi-font - [AUR](https://aur.archlinux.org/packages/bdf-tewi-git/)

* ttf-mplus - [**"FREE"**](http://mplus-fonts.osdn.jp/about-en.html#license)
  * http://jaist.dl.osdn.jp/mplus-fonts/62344/mplus-TESTFLIGHT-063.tar.xz - [AUR](https://aur.archlinux.org/packages/ttf-mplus/)

* mplus monospace mod
  * ['M+ 1c'](https://www.fontsquirrel.com/fonts/download/M-1c)
  * ['M+ 1m'](https://www.fontsquirrel.com/fonts/download/M-1m)

* ttf-vlgothic - [**Multiple, custom. Please check readme in archive**]
  * http://jaist.dl.osdn.jp/vlgothic/62375/VLGothic-20141206.tar.xz - [AUR](https://aur.archlinux.org/packages/ttf-vlgothic/)

* ttf-koruri - [**Apache v2**](https://github.com/Koruri/Koruri/blob/master/LICENSE)
  * http://osdn.jp/frs/redir.php?m=iij&f=%2Fkoruri%2F66647%2FKoruri-20161105.tar.xz - [AUR](https://aur.archlinux.org/packages/ttf-koruri/)

## Misc:

* GTK3-mushrooms
  * https://github.com/TomaszGasior/gtk3-mushrooms - [AUR](https://aur.archlinux.org/packages/gtk3-mushrooms/)

* 256 term color test by 'l0b0' - [**GPLv3**](https://github.com/l0b0/xterm-color-count/blob/master/LICENSE.txt)
  * https://github.com/l0b0/xterm-color-count
